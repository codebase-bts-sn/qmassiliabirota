#ifndef MAINUI_H
#define MAINUI_H

#include <QDialog>
#include <QMap>

#include "stationdao.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainUI; }
QT_END_NAMESPACE

class MainUI : public QDialog
{
    Q_OBJECT

public:
    MainUI(QWidget *parent = nullptr);
    ~MainUI();
    void renseignerStations(QMap<int, QString>& lesStations);
    void afficherInfosStation(QList<StationDAO::infosStation_t>& infos);
    void afficherEtatMqtt(bool isConnected);

private:
    Ui::MainUI *ui;

signals:
    void updateInfos(int stationID);

};
#endif // MAINUI_H
