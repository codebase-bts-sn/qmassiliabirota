#ifndef CONNEXIONBDD_H
#define CONNEXIONBDD_H

#include <QSqlDatabase>

class ConnexionBDD
{
public:
    static ConnexionBDD * getInstance();
    bool execute(QSqlQuery& query);
    void close();
private:
    ConnexionBDD();
    ~ConnexionBDD();

    // Rendre inaccessibles les constructeurs de copie/déplacement et opérateurs d'affectation/déplacement
    ConnexionBDD(ConnexionBDD const&) = delete;
    ConnexionBDD(ConnexionBDD&&) = delete;
    ConnexionBDD& operator=(ConnexionBDD const&) = delete;
    ConnexionBDD& operator=(ConnexionBDD &&) = delete;

    static ConnexionBDD * _singleton;
    static int _nbRefs;

    QSqlDatabase _connexionBdd;
};

#endif // CONNEXIONBDD_H
