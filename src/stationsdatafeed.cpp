#include <QJsonDocument>

#include "stationsdatafeed.h"

StationsDataFeed::StationsDataFeed()
    : _mqttClient(nullptr), _subscription(nullptr)
{
    _mqttClient = new QMqttClient();
    _mqttClient->setHostname(_BROKER_URL);
    _mqttClient->setPort(_BROKER_PORT);

    connect(_mqttClient, &QMqttClient::connected, this, &StationsDataFeed::_onMqttConnected);
    connect(_mqttClient, &QMqttClient::messageReceived, this, &StationsDataFeed::_onMqttMsgReceived);

    _mqttClient->connectToHost();
}

void StationsDataFeed::push(QJsonObject value)
{
    QJsonDocument jsonDoc(value);
    const QMqttTopicName topicName = QMqttTopicName(_RTDATAS_TOPIC);

    QByteArray topicValue = jsonDoc.toJson(QJsonDocument::Indented);
    _mqttClient->publish(topicName, topicValue);
}

QJsonObject StationsDataFeed::pull()
{
    return _data;
}

void StationsDataFeed::_onMqttConnected()
{
    _mqttClient->subscribe(_RTDATAS_TOPIC_PATH + "/+");
    emit stateChanged(Ready);
}

void StationsDataFeed::_onMqttMsgReceived(const QByteArray &message, const QMqttTopicName &topic)
{
    QJsonParseError jsonErr;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(message, &jsonErr);

    qDebug() << "jsonErr : " << jsonErr.errorString();

    if( jsonDoc.isObject()) {
        _data = jsonDoc.object();
        qDebug() << "SUCCESS : new JSON object read from topic value";

        // Récupérer l'identifiant de la station dont les données sont mises à jour
        // depuis le dernier niveau du topic MQTT
        QStringList topicLevels = topic.name().split('/');
        QString topicName = topicLevels.last();
        qDebug() << "topic name : " << topicName;
        bool isOK;
        int stationID = topicName.toInt(&isOK);

        // Récupérer le topic parent de la station dont les données sont mises à jour
        topicLevels.removeLast();
        QString topicPath = topicLevels.join('/');
        qDebug() << "topic path : " << topicPath;

        // SI le topic est conforme à ce qui est attendu ALORS
        if((topicPath == _RTDATAS_TOPIC_PATH) && isOK) {
            // On émet un signal pour prévenir de la MàJ des données pour cette station
            emit infoReady(stationID);
        }
    } else {
         qDebug() << "ERROR : topic value is not a JSON object !";
    }

}


