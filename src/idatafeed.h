#ifndef IDATAFEED_H
#define IDATAFEED_H

#include <QObject>
#include <QJsonObject>

class IDataFeed : public QObject
{
    Q_OBJECT

public:
    explicit IDataFeed(QObject *parent = nullptr);
    virtual void push(QJsonObject value) = 0;
    virtual QJsonObject pull() = 0;
    typedef enum {Ready, Busy, Error} feedstate_t;

signals:
    void dataReady(void);
    void stateChanged(feedstate_t state);
};

#endif // IDATAFEED_H
