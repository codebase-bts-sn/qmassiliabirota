#include <QDebug>
#include <QHash>
#include <QJsonObject>

#include "app.h"

App::App(QObject *parent)
    : QObject{parent}
{
    connect(&_ihm, &MainUI::updateInfos, this, &App::onIhmUpdateInfos);
    connect(&_dataFeed, &StationsDataFeed::stateChanged, this, &App::onDatafeedStateChanged);
    connect(&_dataFeed, &StationsDataFeed::infoReady, this, &App::onStationInfoReady);
}

void App::run()
{
    _ihm.show();

    QMap<int, QString> nomsStations;
    if(_station.getNames(nomsStations)) {
        _ihm.renseignerStations(nomsStations);
    }

    onIhmUpdateInfos(nomsStations.firstKey());
}

void App::onIhmUpdateInfos(int stationID)
{
    _currentStationID = stationID;
    QList<StationDAO::infosStation_t> infos;
    _station.getLastInfosByStationID(stationID, infos);

    _ihm.afficherInfosStation(infos);
}

void App::onDatafeedStateChanged(StationsDataFeed::feedstate_t state)
{
    if(state == StationsDataFeed::Ready) {
        _ihm.afficherEtatMqtt(true);
    } else {
        _ihm.afficherEtatMqtt(false);
    }
}

void App::onStationInfoReady(int stationID)
{
    // Récupérer les donnée de station reçues du broker MQTT
    QJsonObject stationInfos = _dataFeed.pull();

    // Mettre à jour la BDD
    _station.setInfosByStationID(stationID, stationInfos);

    // Mettre à jour l'affichage si l'ID de station correspond à celui actuellement affiché
    if( stationID == _currentStationID) {
        onIhmUpdateInfos(stationID);
    }


}
