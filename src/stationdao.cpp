#include <QSqlQuery>
#include <QStringList>
#include <QVariant>
#include <QSqlError>
#include <QDebug>

#include "stationdao.h"

StationDAO::StationDAO()
{
    _bdd = ConnexionBDD::getInstance();
}

bool StationDAO::getNames(QMap<int, QString> &noms)
{
    QSqlQuery req;
    bool ret = false;

    req.prepare("SELECT Number, Name FROM stations;");

    if(req.exec()) {
        ret = true;

        while(req.next()) {
            noms[req.value("Number").toInt()] = req.value("Name").toString();
        }
    } else {
        qDebug() << req.lastError().text();
    }

    return ret;
}

bool StationDAO::getLastInfosByStationID(int stationID, QList<infosStation_t> &infos, int limit)
{
    QSqlQuery req;
    bool ret = false;

    req.prepare("SELECT d.isOpened, d.Capacity, d.AvailableBikes, d.AvailableStands, d.LastUpdate FROM datas as d"
                " JOIN stations as s ON d.stations_Number = s.Number"
                " WHERE s.Number = :stationID"
                " ORDER BY d.LastUpdate DESC"
                " LIMIT :limit;"
                );
    req.bindValue(":stationID", stationID);
    req.bindValue(":limit", limit);


    if(req.exec()) {
        ret = true;

        while(req.next()) {
            infosStation_t i;

            i.isOuvert = req.value("isOpened").toBool();
            i.capacite = req.value("Capacity").toInt();
            i.velosDispos = req.value("AvailableBikes").toInt();
            i.rateliersDispos = req.value("AvailableStands").toInt();
            QDateTime dt;
            dt.setTime_t(req.value("LastUpdate").toInt());
            i.dateInfos = dt;
            infos.append(i);
        }
    } else {
        qDebug() << req.lastError().text();
    }

    return ret;
}

bool StationDAO::setInfosByStationID(int stationID, QJsonObject &infos)
{
    QJsonObject disponibilites = infos["Availability"].toObject();
    qDebug() << "Station ID : " << stationID;
    int capacite = infos["Capacity"].toInt();
    int velosDispos = disponibilites["Bikes"].toInt();
    int rateliersDispos = disponibilites["Stands"].toInt();
    int horodatage = infos["LastUpdate"].toInt();
    bool isOuvert = infos["isOpened"].toBool();

    qDebug() << ". Capacité              : " << capacite;
    qDebug() << ". Râteliers disponibles : " << rateliersDispos;
    qDebug() << ". Vélos disponibles     : " << velosDispos;
    qDebug() << ". Date MàJ infos        : " << horodatage;


    QSqlQuery req;
    bool ret = false;

    req.prepare("INSERT INTO datas(isOpened, Capacity, AvailableBikes, AvailableStands, LastUpdate, stations_Number)"
                " VALUES(:isOuvert, :capacite, :velosDispos, :rateliersDispos, :horodatage, :fkStations);"
                );
    req.bindValue(":isOuvert", isOuvert);
    req.bindValue(":capacite",capacite);
    req.bindValue(":velosDispos",velosDispos);
    req.bindValue(":rateliersDispos",rateliersDispos);
    req.bindValue(":horodatage",horodatage);
    req.bindValue(":fkStations", stationID);


    if(req.exec()) {
        ret = true;
    } else {
        qDebug() << req.lastError().text();
    }

    return ret;
}
