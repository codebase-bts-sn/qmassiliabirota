#include <QDebug>
#include <QSqlQuery>

#include "connexionbdd.h"

// Initialisation des attributs static
ConnexionBDD * ConnexionBDD::_singleton = nullptr;
int ConnexionBDD::_nbRefs = 0;

ConnexionBDD * ConnexionBDD::getInstance()
{
    if (_singleton == nullptr) {
           _singleton = new ConnexionBDD();
        }

       _nbRefs++;

       return _singleton;
}

bool ConnexionBDD::execute(QSqlQuery &query)
{
    return true;
}

void ConnexionBDD::close()
{
    _nbRefs--;
    if(_nbRefs == 0) {
       delete _singleton;
       _singleton = nullptr;
    }
}

ConnexionBDD::ConnexionBDD()
{
    _connexionBdd = QSqlDatabase::addDatabase("QSQLITE");
    _connexionBdd.setDatabaseName("./dbmassiliabirota.sqlite3");

    if(_connexionBdd.open()) {
        qDebug() << "BDD ouverte";
    } else {
        qDebug() << "Erreur ouverture BDD";
    }
}

ConnexionBDD::~ConnexionBDD()
{
    _connexionBdd.close();
}
