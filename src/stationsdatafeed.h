#ifndef SENSORSDATAFEED_H
#define SENSORSDATAFEED_H

#include <QMqttClient>
#include <QMqttTopicFilter>

#include <idatafeed.h>

class StationsDataFeed : public IDataFeed
{
    Q_OBJECT
public:
    StationsDataFeed();
    void push(QJsonObject value) override;
    QJsonObject pull() override;

private:
    QMqttClient * _mqttClient;
    QMqttSubscription * _subscription;
    const QString _BROKER_URL = "localhost";
//    const QString _BROKER_URL = "broker.hivemq.com";
//    const QString _BROKER_URL = "rpi-defrance.local";
//    const QString _BROKER_URL = "192.168.5.124";
    const int _BROKER_PORT = 1883;
    const QString _RTDATAS_TOPIC = "MassiliaBirota/rtdatas/+";
    const QString _RTDATAS_TOPIC_PATH = "MassiliaBirota/rtdatas";

    QJsonObject _data;
    /* Format attendu :
{
    "isOpened" : true,
    "Capacity" : 10,
    "Availability" : {
        "Stands" : 7,
        "Bikes" : 3
    },
    "LastUpdate" : 1535203109
}
     */

signals :
    void infoReady(int stationID);

private slots:
    void _onMqttConnected();
    void _onMqttMsgReceived(const QByteArray &message, const QMqttTopicName &topic);

};

#endif // SENSORSDATAFEED_H
