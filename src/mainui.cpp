#include <QDebug>

#include "mainui.h"
#include "ui_mainui.h"

MainUI::MainUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainUI)
{
    ui->setupUi(this);

    connect(ui->cbxStations, QOverload<int>::of(&QComboBox::currentIndexChanged), [this]() {emit updateInfos(ui->cbxStations->currentData().toInt());});
    // Voir doc de QCombobox pour la syntaxe "spéciale" utilisée pour définir le signal
    // (elle vient du fait que le signal currentIndexChanged est surchargé dans cette classe)

    QStringList entetesColonnes;
    entetesColonnes.append("Vélos disponibles");
    entetesColonnes.append("Râteliers disponibles");
    entetesColonnes.append("Capacité station");
    entetesColonnes.append("Date de MàJ");
    ui->tableWidget->setHorizontalHeaderLabels(entetesColonnes);
}

MainUI::~MainUI()
{
    delete ui;
}

void MainUI::renseignerStations(QMap<int, QString>& lesStations)
{
    QMapIterator<int, QString> it(lesStations);
    ui->cbxStations->clear();
    qDebug() <<  "size : " << lesStations.size();
    while (it.hasNext()) {
        it.next();
        ui->cbxStations->addItem(it.value(), it.key());
    }
}

void MainUI::afficherInfosStation(QList<StationDAO::infosStation_t> &infos)
{
    ui->lblOuvertFerme->setText(infos.at(0).isOuvert ? "Ouverte" : "Fermée");

    int idxRow = 0;
    ui->tableWidget->setRowCount(0); // Efface toutes les lignes déjà présentes

    for(StationDAO::infosStation_t info : infos) {
        ui->tableWidget->insertRow(idxRow);

        ui->tableWidget->setItem(idxRow, 0, new QTableWidgetItem(QString::number(info.velosDispos)));
        ui->tableWidget->setItem(idxRow, 1, new QTableWidgetItem(QString::number(info.rateliersDispos)));
        ui->tableWidget->setItem(idxRow, 2, new QTableWidgetItem(QString::number(info.capacite)));
        ui->tableWidget->setItem(idxRow, 3, new QTableWidgetItem(info.dateInfos.toString()));

        idxRow++;
    }
    ui->tableWidget->resizeColumnsToContents();
}

void MainUI::afficherEtatMqtt(bool isConnected)
{
    QString status = isConnected ? "Connecté" : "Déconnecté";
    ui->lblBrokerStatus->setText(status);
}

