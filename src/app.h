#ifndef APP_H
#define APP_H

#include <QSqlDatabase>

#include "mainui.h"
#include "stationdao.h"
#include "stationsdatafeed.h"

#include <QObject>

class App : public QObject
{
    Q_OBJECT
public:
    explicit App(QObject *parent = nullptr);
    void run();

private :
    MainUI _ihm;
    StationDAO _station;
    StationsDataFeed _dataFeed;
    int _currentStationID;

private slots:
    void onIhmUpdateInfos(int stationID);
    void onDatafeedStateChanged(StationsDataFeed::feedstate_t state);
    void onStationInfoReady(int stationID);

};

#endif // APP_H
