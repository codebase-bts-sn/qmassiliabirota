#ifndef STATIONDAO_H
#define STATIONDAO_H

#include <QDateTime>
#include <QMap>
#include <QJsonObject>

#include "connexionbdd.h"

class StationDAO
{
public:
    StationDAO();
    typedef struct {
        bool isOuvert;
        int capacite;
        int velosDispos;
        int rateliersDispos;
        QDateTime dateInfos;
    } infosStation_t;
    bool getNames(QMap<int, QString>& noms);
    bool getLastInfosByStationID(int stationID, QList<infosStation_t> &infos, int limit = 10);
    bool setInfosByStationID(int stationID, QJsonObject& infos);
private:
    ConnexionBDD * _bdd;
};

#endif // STATIONDAO_H
