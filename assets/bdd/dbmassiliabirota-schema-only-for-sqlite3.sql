--
-- File generated with SQLiteStudio v3.4.4 on sam. mars 9 10:19:08 2024
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: datas
DROP TABLE IF EXISTS datas;

CREATE TABLE IF NOT EXISTS datas (
    data_ID         INTEGER PRIMARY KEY AUTOINCREMENT
                            UNIQUE,
    isOpened        INTEGER,
    Capacity        INTEGER,
    AvailableStands INTEGER,
    AvailableBikes  INTEGER,
    LastUpdate      INTEGER,
    stations_Number INTEGER REFERENCES stations (Number) 
);


-- Table: stations
DROP TABLE IF EXISTS stations;

CREATE TABLE IF NOT EXISTS stations (
    Number     INTEGER       PRIMARY KEY
                             UNIQUE,
    Name       VARCHAR (100),
    Address    VARCHAR (255),
    Latitude   FLOAT,
    Longitude  FLOAT,
    isBonus    INTEGER,
    hasBanking INTEGER
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
