# QMassiliaBirota

Application Qt qui s'appuie sur une BDD pour informer sur l'évolution des informations relatives à des stations de vélo en libre service : nombre de vélos et de râteliers disponibles, station ouverte ou non...

La BDD (_SQLite3_) est mise à jour sur publication d'informations sur un _topic_ MQTT. Chaque station dispose de son propre _topic_ dans le _topic_ parent `MassiliaBirota/rtdatas`. Le topic d'un station correspond à la valeur de son identifiant (ex. : `MassiliaBirota/rtdatas/9087`).

Le contenu JSON attendu pour le topic est le suivant :

```json
{
  "isOpened" : true,
  "Capacity" : 10,
  "Availability" : {
    "Stands" : 7,
    "Bikes" : 3
  },
  "LastUpdate" : 1535203109
}
```

Cette application s'inspire de ce que propose l'entreprise JCDecaux à travers son API Web disponible sur [JCDecaux developer](https://developer.jcdecaux.com/#/home).

La nature des informations utilisées par l'application provient de cette API (noms/adresses des stations, informations relatives aux stations...) au moment où celle-ci prenait en charge la ville de Marseille. D'où le nom Massilia Birota (du latin Massilia = Marseille et Birota = Engin à 2 roues). 

Ces informations ne sont plus disponibles depuis 2023 car l'opérateur JCDecaux n'a pas été retenu lors du passage de la flotte de vélos à l'électrique.

![alt text](img/screenshot.png "Qt App screenshot")

## BDD

le modèle de la BDD utilisée est le suivant : 

![alt text](img/dbmassiliabirota-schema.png "Modèle BDD")

Ce modèle est disponible au format _MySQL Workbench_ dans le répertoire `assets/bdd` du dépôt (fichier `dbMassiliaBirota.mwb`).

La BDD proprement dîte est également disponible dans ce même répertoire (`dbmassiliabirota.sqlite3`).

Les fichiers SQL permettant de créer cette BDD sans ou avec données sont repectivement `dbmassiliabirota-schema-only-for-sqlite3.sql` et `dbmassiliabirota-schema-with-data-for-sqlite3.sql`.

## Modèle objet

Le diagramme de classes UML de l'application est le suivant : 

![alt text](img/qmassilia-birota-class-diagram.png "Diagramme de classes")

Le rôle des classes est le suivant :

- `App`  : classe dans laquelle est mise en place la logique métier

- `IDataFeed` : classe **abstraite** (ou **Interface**) qui définit de manière générique, c'est-à-dire sans présumer de la technologie utilisée, les méthodes à implémenter pour recevoir ou envoyer les données relatives aux stations de vélos

- `StationDataFeed` : classe qui implémente les méthodes de la classe abstraite `IDataFeed` pour recevoir les données relatives aux stations de vélos depuis un broker MQTT

- `MainUI` : classe qui prend en charge l'IHM

- `ConnexionBDD` : classe **singleton** qui permet de s'assurer qu'une seule connexion BDD est active lors des accès à celle-ci.

- `StationDAO` : classe qui prend en charge les accès à la BDD pour y extraire ou insérer des données. Le suffixe `DAO` siginifie *Data Access Object*.

## Installation

L'installation sur un hôte *Linux* de type *Debian* se décompose en plusieurs phases :

1. Installation du SDK Qt et de ses modules nécessaires (driver *SQlite3*, module *QtMqtt*) pour compiler l'application

2. Installation de paquets logiciels (*SqLite3*, broker+client MQTT) utilisés par l'application

3. Compilation de l'application

4. Déploiement de la BDD

### Installation de Qt et de ses dépendances

```bash
sudo apt-get update
# Installation du framework Qt et de son IDE
sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools qtcreator
# Installation du driver Qt pour s'interfacer avec SQLite3
sudo apt install libqt5sql5-sqlite
```

Le module *QtMqtt* n'étant pas disponible sous forme binaire, il faut le compiler au préalable :

```bash
# package *indispensable* pour avoir accès au fichier `qobject_p.h` requis lors de la compilation du module `qtmqtt`
sudo apt install qtbase5-private-dev
# installation du système de construction utilisé par le module `qtmqtt`
sudo apt install cmake
# récupère la version de _Qt_ installée (ici *5.15.8*)
qmake --version
QMake version 3.1
Using Qt version 5.15.8 in /usr/lib/x86_64-linux-gnu
# cloner les sources du module `qtmqtt` dans la version la plus proche de la version du framework _Qt_ installé 
# (se rendre par exemple sur https://code.qt.io/cgit/qt/qtmqtt.git/[] pour lister les versions disponibles)
git clone https://code.qt.io/qt/qtmqtt.git --branch 5.15.2
# compiler et installer le module
cd qtmqtt
qmake
make
sudo make install
```

### Installation des paquets logiciels requis par l'application

```bash
# installation de SQLite3
sudo apt install sqlite3 sqlite3-doc
# installation du broker Mosquitto
sudo apt install mosquitto
# autorisation des connexions entrantes sur le port MQTT (-> 1883)
sudo systemctl enable mosquitto.service
sudo ufw allow 1883
```

Pour le client MQTT, on pourra utiliser le logiciel *MQTT.fx* dont la dernière version 1.7.1 gratuite est encore disponible sur internet (Ex. : [mqttfx171-backup](https://github.com/SeppPenner/mqttfx171-backup), [MQTT.Fx 1.7.1 Download](https://mqtt.iot01.com/apps/mqttfx/1.7.1/), [*chocolatey*](https://community.chocolatey.org/packages/mqttfx)...)

### Compilation de l'application

Ouvrir le fichier `src/QMassiliaBirota.pro` dans *QtCreator* et lancer le *build*

### Déploiement de la BDD

Cette étape consiste uniquement à copier le fichier `assets/bdd/dbmassiliabirota.sqlite3` dans le répertoire de l'exécutable de l'application généré par *QtCreator*.

